<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class OrganizationsTableSeeder extends Seeder {

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('organizations')->delete();

        $organizations = array(
            ['id' => 1, 'title' => 'Det Norske Teatret', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'slug' => 'detnorske'],
            ['id' => 2, 'title' => 'Den Norske Opera & Ballett‎', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'slug' => 'opera'],
            ['id' => 3, 'title' => 'Oslo Nye Teater', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'slug' => 'oslonye'],
            ['id' => 4, 'title' => 'Nationaltheatret', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'slug' => 'nationaltheatret'],
            ['id' => 5, 'title' => 'Oslo-Filharmonien', 'created_at' => new DateTime, 'updated_at' => new DateTime, 'slug' => 'filharmonien'],
        );

        // Uncomment the below to run the seeder
        DB::table('organizations')->insert($organizations);
    }

}
