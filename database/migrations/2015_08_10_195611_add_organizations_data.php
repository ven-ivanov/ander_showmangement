<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrganizationsData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			DB::table('organizations')->delete();
			DB::table('organizations')->insert(
		      array(
		          'title' => 'Den Norske Opera & Ballett‎',
		          'slug' => 'opera'
		      )
	    );
			DB::table('organizations')->insert(
					array(
							'title' => 'Det Norske Teatret',
							'slug' => 'detnorske'
					)
			);
			DB::table('organizations')->insert(
					array(
							'title' => 'Oslo Nye Teater',
							'slug' => 'oslonye'
					)
			);
			DB::table('organizations')->insert(
					array(
							'title' => 'Nationaltheatret',
							'slug' => 'nationaltheatret'
					)
			);
			DB::table('organizations')->insert(
					array(
							'title' => 'Oslo-Filharmonien',
							'slug' => 'filharmonien'
					)
			);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
