<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBannerText extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	 public function up()
	 {
	 		Schema::table('shows', function(Blueprint $table)
			{
				$table->string('banner_text');
			});
	 }

 	/**
 	 * Reverse the migrations.
 	 *
 	 * @return void
 	 */
 	public function down()
 	{
 		Schema::table('shows', function(Blueprint $table)
 		{
 			$table->dropColumn('banner_text');
 		});
 	}
}
