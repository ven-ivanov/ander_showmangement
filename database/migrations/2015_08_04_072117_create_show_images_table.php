<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('show_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			//Show Images fields
			$table->string('image_file')->default('');
			$table->string('title')->default('');
			$table->string('description', 500)->default('');

			//show foreign key
			$table->integer('show_id')->unsigned()->default(0);
		});
		Schema::table('show_images', function(Blueprint $table)
		{
			$table->foreign('show_id')->references('id')->on('shows')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('show_images');
	}

}
