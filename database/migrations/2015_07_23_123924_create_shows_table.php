<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shows', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
			/**
			 * shows table fields
			 */
			//title
			$table->string('title')->default('');
			//subtitle
			$table->string('subtitle')->default('');
			//description
			$table->string('description',501)->default('');
			//information link
			$table->string('information_link')->default('');
			//tickets link
			$table->string('tickets_link')->default('');
			//main image
			$table->string('main_image')->default('');
			//promoted to ad
			$table->boolean('promoted_to_ad')->default(false);

			//belongs to organization
			$table->integer('organization_id')->unsigned()->default(0);
			$table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shows');
	}

}
