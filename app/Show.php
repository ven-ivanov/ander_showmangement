<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Show extends Model {

	//
    protected $guarded = [];

    public static function getAllForUser($user) {
      $shows = $user->role == 'administrator'
        ? self::where('id', '!=', 0)
        : self::where('organization_id', $user->organization->id);

      return $shows->orderBy('order_weight', 'asc')
        ->orderBy('id', 'desc')
        ->get();
    }

    public static function getAllForOrganization($organizationId) {
      return self::where('organization_id', $organizationId)
        ->orderBy('order_weight', 'asc')
        ->orderBy('id', 'desc')
        ->get();
    }

    public function showImages()
    {
        return $this->hasMany('App\ShowImage');
    }

    public function showPeople()
    {
        return $this->hasMany('App\ShowPerson');
    }

    public function organization() {
      return $this->belongsTo('App\Organization');
    }

    public function getVideoLinkAttribute($value) {
      $id = '';

      if (strpos($value, 'youtu') !== false) {
        if (preg_match('/watch\?v=([0-9a-z]+)/i', $value, $matches)) {
          $id = $matches[1];
        }
        elseif (preg_match('/youtu\.be\/([0-9a-z]+)/i', $value, $matches)) {
          $id = $matches[1];
        }
        elseif (preg_match('/embed\/([0-9a-z]+)/i', $value, $matches)) {
          $id = $matches[1];
        }

        return '<iframe width="100%" height="344" src="https://www.youtube.com/embed/' . $id . '" frameborder="0"></iframe>';
      }
      else if (strpos($value, 'vimeo') !== false) {
        if (preg_match('/com\/([0-9a-z]+)/i', $value, $matches)) {
          $id = $matches[1];
        }

        return '<iframe src="https://player.vimeo.com/video/136487788" width="100%" height="344" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
      }

      return $value;
    }
}
