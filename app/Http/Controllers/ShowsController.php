<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use Input;
use Redirect;
use App\Show;
use Response;
use App\ShowImage;
use App\ShowPerson;
use App\Organization;
use Illuminate\Contracts\Auth\Guard;

class ShowsController extends Controller {

	protected $rules = [
		'title' => ['required', 'min:3'],
		'subtitle' => ['required', 'min:3'],
		'banner_text' => [],
		'description' => [],
		'information_link' => ['required', 'min:3'],
		'tickets_link' => ['required', 'min:3'],
		'main_image' => ['required', 'min:3'],
	];

	protected $errorMessages = [
		'title.required' => 'Tittel må fylles inn.',
		'subtitle.required' => 'Undertittel må fylles inn.',
		'information_link.required' => 'Lenke til mer info om forestilling må fylles inn.',
		'tickets_link.required' => 'Lenke til billettinformasjon må fylles inn. Om ikke denne finnes, bruk samme som lenke til informasjon.',
		'main_image.required' => 'Et hovedbilde må legges til.',
	];

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}


	/**
	 * Show administration front page.
	 *
	 * @return Response
	 */
	public function index(Guard $user) {
		if ($user->user()->role == 'administrator') {
			$org = Organization::where('id', '!=', 0)->first();

			return redirect()->action('ShowsController@perOrg', [$org->id]);
		}

		$shows = Show::getAllForUser($user->user());

		foreach ($shows as $show) {
			$show->dates_formatted = str_replace(array('[', ']'), '', json_encode(explode(', ', $show->show_dates)));
		}

		return view('shows.index', array(
			'shows' => $shows,
			'orgId' => $user->user()->organization->id,
			'user' => $user->user()
		));
	}

	/**
	 * Show administration front page for specific organization.
	 */
	public function perOrg($org, Guard $user) {
		$org = Organization::find($org);
		$shows = Show::getAllForOrganization($org->id);

		foreach ($shows as $show) {
			$show->dates_formatted = str_replace(array('[', ']'), '', json_encode(explode(', ', $show->show_dates)));
		}

		return view('shows.index', array(
			'shows' => $shows,
			'orgId' => $org->id,
			'user' => $user->user(),
			'orgs' => Organization::all(),
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//create show
		$show = new Show();
		$orgId = isset($_GET['org']) ? $_GET['org'] : '';

		return view('shows.create', array('show' => $show, 'orgId' => $orgId));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Guard $user, Request $request)
	{
		//validates
		$validator = Validator::make($request->all(), $this->rules, $this->errorMessages);

		if ($validator->fails()) {
			return redirect('shows/create')
				->withErrors($validator)
				->withInput(Input::all());
		}
		//save a show
		$input = Input::all();
		unset($input['org']);

		//need to assign organization
		$input['organization_id'] = $user->user()->role == 'administrator' ? Input::get('org') : $user->user()->organization->id;
		$input['order_weight'] = -1;

		parse_str($input['image_form'], $image_form );
		parse_str($input['person_form'],$person_form );

		unset($input['image']);
		unset($input['image_form']);
		unset($input['person_form']);

		//store show information
		$show = Show::create( $input );
		$show_id = $show->id;

		//echo $show_id;

		if($show_id > 0) {
			//store show Image
			//image_form
			if(isset($image_form['title'])){
				$image_count = count($image_form['title']);

				for($i = 0; $i < $image_count; $i++){
					if($i ==0)
						continue;
					//prepare data
					$image_input = array(
						'_token'		=> $image_form['_token'],
						'title'			=> $image_form['title'][$i],
						'description'	=> $image_form['description'][$i],
						'image_file'	=> $image_form['file_path'][$i],
						'show_id'		=> $show_id
					);
					ShowImage::create($image_input);
				}
			}
				//store show Person
				//person_form
				if(isset($person_form['title'])) {

					$person_count = count($person_form['title']);
					for($i = 0; $i < $person_count; $i++){
						if($i ==0)
							continue;
						//prepare data
						$person_input = array(
						'_token'		=> $person_form['_token'],
						'title'			=> $person_form['title'][$i],
						'description'	=> $person_form['description'][$i],
						'image_file'	=> $person_form['file_path'][$i],
						'show_id'		=> $show_id
					);

					ShowPerson::create($person_input);
				}
			}
		}

		if ($user->user()->role == 'administrator') {
			return redirect()->action('ShowsController@perOrg', [$input['organization_id']]);
		}

		return Redirect::route('shows.index')->with('message', 'Project created');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Show $show)
	{
		//show

		return view('shows.show',compact('show'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Show $show)
	{
		//edit show
		//we also get all Images and Persons...
		return view('shows.edit', compact('show'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Show $show, Guard $user, Request $request)
	{
		$validator = Validator::make($request->all(), $this->rules, $this->errorMessages);

		if ($validator->fails()) {
			return redirect('shows/' . $show->id . '/edit')
				->withErrors($validator)
				->withInput(Input::all());
		}

		// Update Show
		$input = array_except(Input::all(), '_method');
		/*print_r($input);
		exit;*/
		//need to assign organization
		$input['organization_id'] = $show->organization->id;

		//by default we put it at the end.
		$input['order_weight'] = -1;

		// extract image and person params
		parse_str($input['image_form'], $image_form );
		parse_str($input['person_form'],$person_form );

		unset($input['image']);
		unset($input['image_form']);
		unset($input['person_form']);

		//update show information
		$show->update($input);
		$show_id = $show->id;

		//Update Removed fields as well...
		foreach($show->showImages as $showImage)
		{
			if(!isset($image_form['id']) || !in_array($showImage->id, $image_form['id']))
			{
				//$showImage->destroy();
				ShowImage::where('id', $showImage->id)->delete();
			}
		}
		foreach($show->showPeople as $showPerson)
		{
			if(!isset($person_form['id']) || !in_array($showPerson->id, $person_form['id']))
			{
				//$showImage->destroy();
				ShowPerson::where('id', $showPerson->id)->delete();
			}
		}

		//saving new our changed...
		if($show_id > 0) {
			//store show Image
			//image_form
			if(isset($image_form['title'])){
				$image_count = count($image_form['title']);

				for($i = 0; $i < $image_count; $i++){
					if($i ==0)
						continue;
					//prepare data
					$image_input = array(
						'_token'		=> $image_form['_token'],
						'title'			=> $image_form['title'][$i],
						'description'	=> $image_form['description'][$i],
						'image_file'	=> $image_form['file_path'][$i],
						'show_id'		=> $show_id
					);
					if(isset($image_form['id'][$i]) && $image_form['id'][$i]>0)
					{
						echo 'updated';
						$image_input['id'] = $image_form['id'][$i];
						$show_image = new ShowImage($image_input);
						$show_image->exists = true;
						$show_image->save();
					}else{
						echo 'saved';
						$show_image = new ShowImage($image_input);
						$show_image->save();
						//ShowImage::create($image_input);
					}

				}
			}
			//store show Person
			//person_form
			if(isset($person_form['title'])) {
				$person_count = count($person_form['title']);
				for($i = 0; $i < $person_count; $i++){
					if($i ==0)
						continue;
					//prepare data
					$person_input = array(
						'_token'		=> $person_form['_token'],
						'title'			=> $person_form['title'][$i],
						'description'	=> $person_form['description'][$i],
						'image_file'	=> $person_form['file_path'][$i],
						'show_id'		=> $show_id
					);
					if(isset($person_form['id'][$i]) && $person_form['id'][$i]>0)
					{
						$person_input['id'] = $person_form['id'][$i];
						$show_person = new ShowPerson($person_input);
						$show_person->exists = true;
						$show_person->save();
					}else{
						$show_person = new ShowPerson($person_input);
						$show_person->save();
						//ShowImage::create($person_input);
					}
				}
			}
		}

		if ($user->user()->role == 'administrator') {
			return redirect()->action('ShowsController@perOrg', [$show->organization->id]);
		}

		return Redirect::route('shows.index')->with('message', 'Show updated.');
	}
	/**
	 * Save Image
	 * @return JSON
	 */
	public function saveImage()
	{
		$file = Input::file('image');
		$input = array('image' => $file);
		$rules = array(
			'image' => 'image'
		);
		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			return Response::json(['success' => false, 'errors' => $validator->getMessageBag()->toArray()]);
		}
		else {
			$destinationPath = 'uploads/';
			$filename = time() . '_' . $file->getClientOriginalName();
			Input::file('image')->move($destinationPath, $filename);

			return Response::json([
				'success' => true,
				'file' => $filename,
				'preview' => '/img/' . $filename . '?w=195&h=110&fit=crop'
			]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Show $show, Guard $user) {
		$org = $show->organization->id;

		ShowPerson::where('show_id', $show->id)->delete();
		ShowImage::where('show_id', $show->id)->delete();
		Show::where('id', $show->id)->delete();

		if ($user->user()->role == 'administrator') {
			return redirect()->action('ShowsController@perOrg', [$org]);
		}

		return Redirect::route('shows.index')->with('message', 'Show deleted.');
	}

	/**
	 * Promote or demote show from being displayed in the ad.
	 */
	public function promote() {
		$id = Input::get('id');
		$status = Input::get('status');

		$show = Show::find($id);

		$show->promoted_to_ad = $status;
		$show->save();

		return Response::json(['success' => true]);
	}

	/**
	 * Promote or demote show from being displayed in the ad.
	 */
	public function order(Guard $user) {
		$weights = Input::get('order');
		$org = Input::get('org');

		if (empty($org)) {
			$org = $user->user()->organization->id;
		}
		
		$shows = Show::getAllForOrganization($org);

		foreach ($shows as $show) {
			$show->order_weight = array_search($show->id, $weights) ? array_search($show->id, $weights) : 0;
			$show->save();
		}

		return Response::json(['success' => true]);
	}
}
