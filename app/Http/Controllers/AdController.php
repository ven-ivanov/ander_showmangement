<?php namespace App\Http\Controllers;

use App\Show;
use App\ShowImage;
use App\ShowPerson;
use App\Organization;

class AdController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the ad.
	 *
	 * @return Response
	 */
	public function index()
	{
		$orgs = Organization::all()->all();
		shuffle($orgs);
		$adShows = array(array(), array(), array());

		foreach ($orgs as $index => $org) {
			$shows = Show::where('promoted_to_ad', 1)
				->where('organization_id', $org->id)
				->orderBy('order_weight', 'asc')
				->orderBy('id', 'desc')
				->take(3)
				->get()
				->all();

			if (count($shows) == 0) {
				$shows = Show::where('organization_id', $org->id)
					->orderBy('order_weight', 'asc')
					->orderBy('id', 'desc')
					->take(3)
					->get()
					->all();
			}

			if (count($shows) == 0) {
				continue;
			}

			$adShows[0][] = $shows[0];
			$adShows[1][] = isset($shows[1]) ? $shows[1] : $shows[0];
			$adShows[2][] = isset($shows[2]) ? $shows[2] : $shows[count($shows) - 1];
		}

		$shows = array_merge($adShows[0], $adShows[1], $adShows[2]);
		$data = array();

		foreach ($shows as $show) {
			$info = array(
				'title' => $show->title,
				'subtitle' => $show->subtitle,
				'teaser' => $show->description,
				'image' => 'img/' . $show->main_image . '?w=614&h=345&fit=crop',
				'backImage' => 'img/' . $show->main_image . '?w=614&h=345&fit=crop',
				'infoLink' => $show->information_link,
				'buyLink' => $show->tickets_link,
				'scene' => $show->organization->slug,
				'video' => $show->video_link,
				'bannerText' => $show->banner_text,
				'infoSections' => array(),
			);

			foreach ($show->showPeople as $person) {
				if (!empty($person->title)) {
					$info['infoSections'][] = array(
						'type' => 'person',
						'title' => $person->title,
						'description' => $person->description,
						'image' => 'img/' . $person->image_file . '?w=60&h=60&fit=crop',
					);
				}
			}

			foreach ($show->showImages as $image) {
				if (!empty($image->title)) {
					$info['infoSections'][] = array(
						'type' => 'image',
						'title' => $image->title,
						'description' => $image->description,
						'image' => 'img/' . $image->image_file . '?w=241&h=134&fit=crop',
					);
				}
			}

			$data[] = $info;
		}

		return view('ad', ['shows' => json_encode($data)]);
	}

}
