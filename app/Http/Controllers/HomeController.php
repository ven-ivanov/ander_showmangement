<?php namespace App\Http\Controllers;

use App\Show;
use App\ShowImage;
use App\ShowPerson;
use App\Organization;
use DB;
use Input;

class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
		// $this->middleware('guest');
	}

	/**
	 * Show the front page with shows.
	 *
	 * @return Response
	 */
	public function index()
	{
		$organizations = Organization::all();
		$shows = Show::orderByRaw("RAND()")->get();

		return view('home', array('orgs' => $organizations, 'shows' => $shows));
	}

	/**
	 * Send a narrowed down selection of shows.
	 *
	 * @return Response
	 */
	public function selection()
	{
		$date = Input::get('date');
		$org = Input::get('org');
		$show = Input::get('show');

		if (isset($show) && is_numeric($show)) {
			$shows = array(Show::find($show));
		}
		else {
			$shows = Show::orderByRaw("RAND()");

			if (isset($org) && is_numeric($org)) {
				$shows->where('organization_id', $org);
			}

			if (isset($date) && !empty($date)) {
				$dates = explode(', ', $date);

				foreach ($dates as $date) {
					$shows->where(function ($query) use ($dates)
					{
					    foreach ($dates as $value)
					    {
					        $query->orWhere('show_dates', 'LIKE', '%' . $value . '%');
					    }
					});
				}
			}

			$shows = $shows->get();
		}

		return view('shows', array('shows' => $shows));
	}

}
