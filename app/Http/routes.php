<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::post('/get-selection', 'HomeController@selection');

Route::get('/ad', 'AdController@index');

Route::post('/upload/showimage','ShowImagesController@saveImage');
Route::post('/upload/peopleimage','ShowPersonsController@saveImage');
Route::post('/upload/mainimage','ShowsController@saveImage');

Route::post('/shows/promote','ShowsController@promote');
Route::post('/shows/order','ShowsController@order');

Route::get('/shows/{show}/delete', 'ShowsController@delete');

Route::get('/shows/admin/{org}', 'ShowsController@perOrg');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::model('shows', 'Show');
Route::model('showpersons', 'ShowPerson');
Route::model('showimages', 'ShowImage');

Route::resource('shows', 'ShowsController');
Route::resource('shows.showpersons', 'ShowPersonsController');
Route::resource('shows.showimages', 'ShowImagesController');


Route::bind('shows', function($value, $route) {
	return App\Show::whereId($value)->first();
});

Route::get('/admin', function() {
	return Redirect::to('/shows');
});

Route::get('img/{path}', function(League\Glide\Server $server, $path) {
	$server->outputImage($path, $_GET);
});
