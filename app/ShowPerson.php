<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowPerson extends Model {

	//
    protected $guarded = [];
    public function show()
    {
        return $this->belongsTo('App\Show');
    }
}
