<header id="ap-header">
    <a href="/shows"><img src="/images/ap-header.png"></a>
</header>
<section id="header">
    <div class="row">
        <div class="small-12">
            <div class="small-12 medium-4 columns">
                <a href="/"><img src="/images/pascenen_logo.svg" alt="På Scenen" style="width:100%;"></a>
                <a href="/"><h5>www.pascenen.no</h5></a>
            </div>
            <div class="small-12 medium-8 columns">
                <nav>
                    <ul class="small-block-grid-2 medium-block-grid-5">
                        <li>
                            <a href="http://www.detnorsketeatret.no/"><img src="/images/logos/landingpage/detnorsketeatret.png" alt="Det Norske Teatret"></a>
                        </li>
                        <li>
                            <a href="http://operaen.no/"><img src="/images/logos/landingpage/operaen.png" alt="Den Norske Opera"></a>
                        </li>
                        <li>
                            <a href="http://oslonye.no/"><img src="/images/logos/landingpage/oslonye.png" alt="Oslo Nye"></a>
                        </li>
                        <li>
                            <a href="http://www.oslo-filharmonien.no/"><img src="/images/logos/landingpage/filharmonien.png" alt="Filharmonien"></a>
                        </li>
                        <li>
                            <a href="http://www.nationaltheatret.no/"><img src="/images/logos/landingpage/nationaltheatret.png" alt="National Theatret"></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>
