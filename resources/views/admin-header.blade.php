<header id="ap-header">
    <img src="/images/ap-header.png">
</header>

<section id="header">
    <div class="row">
        <div class="small-12">
            <div class="small-12 medium-3 columns">
                <a href="/"><img src="/images/pascenen_logo.svg" alt="På Scenen" style="width:100%;"></a>
            </div>
            <div class="small-12 medium-3 columns">
                <h5 class="padded"><a href="/shows">Administrasjon</a></h5>
            </div>
            <div class="small-10 medium-6 columns text-right">
                <a href="/auth/logout" class="padded inline">Logg ut</a>
                @if ($currentUser->role != 'administrator')<h5 class="padded inline"><strong>{{ $currentUser->organization->title }}</strong></h5>@endif
            </div>
        </div>
    </div>
</section>
