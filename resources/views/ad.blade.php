<!DOCTYPE html>
<html>
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>På Scenen</title>
  <meta name="description" content="">
  <meta name="author" content="Savant AS">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <style>
  /*
  * Skeleton V2.0.4
  * Copyright 2014, Dave Gamache
  * www.getskeleton.com
  * Free to use under the MIT license.
  * http://www.opensource.org/licenses/mit-license.php
  * 12/29/2014
  */

  /* cyrillic-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTQ7aC6SjiAOpAWOKfJDfVRY.woff2) format('woff2');
  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
}
/* cyrillic */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTRdwxCXfZpKo5kWAx_74bHs.woff2) format('woff2');
  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTZ6vnaPZw6nYDxM4SVEMFKg.woff2) format('woff2');
  unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTfy1_HTwRwgtl1cPga3Fy3Y.woff2) format('woff2');
  unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTfgrLsWo7Jk1KvZser0olKY.woff2) format('woff2');
  unicode-range: U+0102-0103, U+1EA0-1EF1, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTYjoYw3YTyktCCer_ilOlhE.woff2) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(http://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTRampu5_7CjHW5spxoeN3Vs.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/K88pR3goAWT7BTt32Z01m4X0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
}
/* cyrillic */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/RjgO7rYTmqiVp7vzi-Q5UYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/LWCjsQkB6EMdfHrEVqA1KYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
  unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/xozscpT2726on7jbcb_pAoX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
  unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/59ZRklaO5bWGqF5A9baEEYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
  unicode-range: U+0102-0103, U+1EA0-1EF1, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/u-WUoqrET9fUeobQW7jkRYX0hVgzZQUfRDuZrPvH3D8.woff2) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(http://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3ZBw1xU1rKptJj_0jans920.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzA7aC6SjiAOpAWOKfJDfVRY.woff2) format('woff2');
  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
}
/* cyrillic */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzBdwxCXfZpKo5kWAx_74bHs.woff2) format('woff2');
  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* greek-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzJ6vnaPZw6nYDxM4SVEMFKg.woff2) format('woff2');
  unicode-range: U+1F00-1FFF;
}
/* greek */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzPy1_HTwRwgtl1cPga3Fy3Y.woff2) format('woff2');
  unicode-range: U+0370-03FF;
}
/* vietnamese */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzPgrLsWo7Jk1KvZser0olKY.woff2) format('woff2');
  unicode-range: U+0102-0103, U+1EA0-1EF1, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzIjoYw3YTyktCCer_ilOlhE.woff2) format('woff2');
  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(http://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzBampu5_7CjHW5spxoeN3Vs.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
}

  #body {
    scrollbar-face-color: #222;
    scrollbar-shadow-color: #222;
    scrollbar-highlight-color:#ccc;
    scrollbar-3dlight-color: #ccc;
    scrollbar-darkshadow-color: #2D2C2D;
    scrollbar-track-color: #7D7D7D;
    scrollbar-arrow-color: #C1C1C1;
    overflow: hidden;
  }

  .container {
      position: relative;
      max-width: 940px;
      margin: 0 0 0 2rem;
      box-sizing: border-box;
      width: 90%;
  }

  .container.collapse {
      margin: 1rem 0 0 20px;
  }

  .column,
  .columns {
    width: 100%;
    float: left;
    box-sizing: border-box;
    }

  .row {
      max-width: 940px;
      height: 100%;
      margin: 0;
  }

  #header {
    padding-top: 30px;
    margin-bottom: 30px;
  }

  .navigation {
      display: block;
  }

  .logo:hover, .logo.active {
      opacity: 1;
  }

  .header-link {
    display: block;
    overflow: hidden;
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 9;
    padding-top: 2rem;
    margin-top: -2rem;
  }

  #main-content {
    height: 344px;
  }

  .frame {
      border: 1px solid #d1a040;
  }

  .coverimage {
      width: 100%;
  }

  #banner img {
      width: 100%;
  }

  .filter {
      background-color:#222;
      padding: 1rem 2rem;
      width: 100%;
      margin-top: 1rem;
  }

  .arrow {
      height:38px;
      margin-top:14rem;
      display:block;
  }

  .arrow:hover {
      opacity: 0.4;
  }

  .actor {
      display: block;
      background-color: #222;
      padding: 1rem 1rem 0;
      width: 100%;
      height: auto;
      margin-bottom: 1rem;
      box-sizing: border-box;
  }

  .button.break {
      display: inline-block;
      margin: 0 1rem 0 0;
  }

  .button.break:last-of-type {
    margin-right: 0;
  }

  /* Base Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  /* NOTE
  html is set to 62.5% so that all the REM measurements throughout Skeleton
  are based on 10px sizing. So basically 1.5rem = 15px :) */


  #body {
    font-size: 15px; /* currently ems cause chrome bug misinterpreting rems on body element */
    line-height: 1.6;
    font-weight: 400;
    font-family: 'Open Sans', sans-serif;
    color: #fff;
    background-color: #000;
    margin: 0;
    height: 500px;
    width: 980px;
    }

    #banner {
      height: 500px;
      width: 980px;
    }

    #banner a {
      color: #d1a040;
      text-decoration: none;
    }

    #banner a span {
      text-decoration: underline;
    }


  /* Typography
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  #banner h1, #banner h2, #banner h3, #banner h4, #banner h5, #banner h6 {
    margin-top: 0;
    margin-bottom: 0.5rem;
  }

  #banner h1 {
      font-size: 3rem;
      line-height: 1;
      font-weight: 700;
      text-transform: uppercase;
  }

  #banner h2 {
      font-size: 1.4rem;
      line-height: 1;
      font-weight: 700;
      text-transform: uppercase;
      margin-bottom: 0.2rem;
      }

  #banner h3 {
      font-size: 1.4rem;
      line-height: 1.5;
      font-weight: 400;
      margin-bottom: 3rem;
      }

  #banner h4 {
      font-size: 1.4rem;
      line-height: 1.5;
      font-weight: 300;
      color: #b3b3b3;
      }

  #banner h5 {
      font-size: 1.4rem;
      line-height: 1.5;
      font-weight: 300;
      color: #d1a040;
      }

  #banner h6 {
      font-size: 1.1rem;
      line-height: 1.5;
      font-weight: 400;
      margin: 0;
      }

  #banner p {
      margin-top: 0.5rem;
      font-size: 1rem;
      line-height: 1.5;
      font-weight: 400;
      }


  /* Buttons
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .button {
    display: block;
    height: 38px;
    width: 145px;
    padding: 0 30px;
    color: #fff;
    text-align: center;
    font-size: 1.4rem;
    font-weight: 700;
    line-height: 35px;
    text-transform: uppercase;
    text-decoration: none;
    white-space: nowrap;
    background-color: transparent;
    border: 1px solid #d1a040;
    cursor: pointer;
    box-sizing: border-box;
    }

  .button:hover {
    outline: 0;
    border-width: 3px;
    line-height: 32px;
  }


  /* Spacing
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  button,
  .button,
  select {
    margin: 1rem 0; }


  /* Utilities
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .u-full-width {
    width: 80%;
    box-sizing: border-box; }


  /* Clearing
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  /* Self Clearing Goodness */
  .container:after,
  .row:after,
  .u-cf {
    content: "";
    display: table;
    clear: both;
  }


  /* Slider */
  #slider-viewport {
    width: 940px;
    overflow: hidden;
  }

  #slides {
    width: 4800px;
    position: relative;
    left: 0;
    transition: left .5s ease-in-out;
  }

  #slider-viewport .slide {
    float: left;
    position: relative;
    height: 344px;
    width: 100%;
  }

  .slider-nav {
    position: absolute;
    top: 165px;
    left: 10px;
    width: 30px;
    height: 30px;
    background: url(/images/arrow_left.svg) center center no-repeat;
    background-size: 100% 100%;
    z-index: 99;
    cursor: pointer;
    opacity: .6;
    transition: opacity .3s;
  }

  .slider-nav:hover {
    opacity: 1;
  }

  #slider-next {
    left: auto;
    right: 10px;
    background: url(/images/arrow_right.svg) center center no-repeat;
  }

  .video {
    position: relative;
    cursor: pointer;
    overflow: hidden;
  }

  .video:before {
    content: "";
    display: block;
    position: absolute;
    width: 90px;
    height: 90px;
    left: 275px;
    top: 135px;
    background: url(/images/play.svg) center center no-repeat;
    background-size: 100% 100%;
  }

  .special {
    background-color: #A80024;
    position: absolute;
    top: 5rem;
    right: -5rem;
    transform: rotate(37deg);
    width: 28rem;
    text-align: center;
    font-size: 1.7rem;
    line-height: 3.4rem;
  }

  .info-sidebar {
    padding-left: 10px;
    padding-right: 10px;
    height: 330px;
    overflow: auto;
  }

  .info-sidebar .info p {
    font-size: 1.3rem;
  }

  .buttons {
    position: absolute;
    bottom: 0;
    background: black;
    padding-top: 10px;
    padding-right: 3px;
  }

  #close {
    position: absolute;
    left: 0;
    top: -15px;
    z-index: 99;
    border: 1px solid #d1a040;
    padding: 5px 5px;
    line-height: 15px;
    border-radius: 50%;
    font-size: 15px;
    width: 15px;
    height: 15px;
    text-align: center;
  }

  #close:hover {
    color: white;
  }


  /* Larger than phablet */
  @media (min-width: 500px) {

      .logo {
          width: 16%;
          margin: 0 0.5rem 0 0;
          display: inline-block;
      }

      .buttoncontainer {
          width: 100%;
          float: left;
          margin-bottom: 2rem;
      }

      #banner h3 {
          margin-bottom: 0;
      }

      html {
        font-size: 62.5%;
      }
  }

  /* Larger than max width */
  @media (min-width: 940px) {
      .navigation {
        position: relative;
      }

      .logo {
        width: 13%;
        display: inline-block;
        margin-left: 1rem;
        opacity: 0.4;
        margin-top: 1px;
        text-align: center;
      }

      .logo:first-child {
        margin-left: 60px;
      }

      .logo img {
        max-height: 38px;
        width: auto !important;
      }

      #detnorske img {
        max-height: 32px;
        position: relative;
        top: -5px;
      }

      #opera {
        width: 8%;
        margin-right: 1rem;
        margin-left: 0;
        position: relative;
      }

      #opera img {
        max-height: 48px;
        position: relative;
        top: 3px;
      }

      #filharmonien img {
        position: relative;
        top: 5px;
        max-height: 48px
      }

      .buttoncontainer {
          float: right;
          margin: 0;
          width: 30%;
      }

      .container {
        width: 100%; }
      .column,
      .columns {
        margin-left: 1rem;    }
      .column:first-child,
      .columns:first-child {
        margin-left: 0; }

        .one.column,
        .one.columns                    { width: 4.66666666667%; }
        .two.columns                    { width: 13.3333333333%; }
        .three.columns                  { width: 22%;            }
        .four.columns                   { width: 33.33332%;      }
        .five.columns                   { width: 39.3333333333%; }
        .six.columns                    { width: 48%;            }
        .seven.columns                  { width: 56.6666666667%; }
        .eight.columns                  { width: 65.3333333333%; }
        .nine.columns                   { width: 74.0%;          }
        .ten.columns                    { width: 82.6666666667%; }
        .eleven.columns                 { width: 91.3333333333%; }
        .twelve.columns                 { width: 100%; margin-left: 0; }

        ::-webkit-scrollbar
        {
          width: 12px;  /* for vertical scrollbars */
          height: 12px; /* for horizontal scrollbars */
        }

        ::-webkit-scrollbar-track
        {
          background: rgba(255, 255, 255, 0.1);
        }

        ::-webkit-scrollbar-thumb
        {
          background: rgba(255, 255, 255, 0.5);
        }

  }
  </style>

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body id="body">
  <div id="banner" onclick="openWindow();">
    <div class="container collapse" id="header">
        <a href="../" target="_blank" class="header-link"></a>
        <div class="row">
            <div class="twelve columns">
                <div class="three columns">
                    <img src="/images/pascenen_logo.svg" alt="På Scenen" style="width:100%;">
                    <h5><a href="http://www.pascenen.no">Alle forestillinger - <span>pascenen.no</span></a></h5>
                </div>
                <div class="navigation">
                    <div class="logo" id="detnorske"><img src="/images/logos/detnorsketeatret.png" alt="Det Norske Teatret"></div>
                    <div class="logo" id="opera"><img src="/images/logos/operaen.png" alt="Den Norske Opera"></div>
                    <div class="logo" id="oslonye"><img src="/images/logos/oslonye.png" alt="Oslo Nye"></div>
                    <div class="logo" id="filharmonien"><img src="/images/logos/filharmonien.png" alt="Filharmonien"></div>
                    <div class="logo" id="national"><img src="/images/logos/nationaltheatret.png" alt="National Theatret"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="container collapse" id="main-content">
      <div id="slides-wrapper"></div>
      <div id="backside-wrapper" style="display: none"></div>
    </div>

  </div>

    <script>
      var shows = {!! $shows !!} ;
      var shows2 = [
        {
          title: 'Brand',
          subtitle: 'Av Henrik Ibsen',
          teaser: 'Med Svein Tindberg, Agnes Kittelsen og Mads Ousdal',
          video: '<iframe width="100%" height="344" src="https://www.youtube.com/embed/YaSPuaqzQ1g" frameborder="0"></iframe>',
          image: 'images/brand.jpg',
          infoLink: 'http://www.detnorsketeatret.no/framsyningar/brand/',
          buyLink: 'http://www.detnorsketeatret.no/framsyningar/brand/',
          scene: 'detnorske',
          infoSections: [
            {
              type: 'person',
              image: 'images/people/agnes.jpg',
              title: 'Agnes Kittelsen',
              subtitle: 'I rollen som Agnes',
              description: 'Tidligere skuespill:<br>2015 - Bu Betre (Ho1)'
            },
            {
              type: 'person',
              image: 'images/people/svein.jpg',
              title: 'Svein Tindberg',
              subtitle: 'I rollen som Agnes',
              description: 'Tidligere skuespill:<br>2015 - Abrahams Barn'
            },
            {
              type: 'person',
              image: 'images/people/svein.jpg',
              title: 'Mads Ousdal',
              subtitle: 'I rollen som Einar',
              description: 'Tidligere skuespill:<br>2014 - Hamlet (Claudius)'
            }
          ]
        },
        {
          title: 'M¡longa',
          subtitle: 'Et nytt blikk på tango',
          teaser: 'Tango med ny vri fra Sidi Larbi Cherkaoui',
          video: '<iframe width="100%" height="344" src="https://www.youtube.com/embed/GLxAg04Hkuo" frameborder="0"></iframe>',
          image: 'images/milonga.jpg',
          infoLink: 'http://operaen.no/Forestillinger/milonga--et-nytt-blikk-pa-tango',
          buyLink: 'http://operaen.no/Forestillinger/milonga--et-nytt-blikk-pa-tango/#sectionKjop',
          scene: 'opera',
          infoSections: [
            {
              type: 'text',
              text: "<h4>Tango for tolv!</h4>"
              + "<p>Tango for tolv! På scenen i m¡longa – et nytt blikk på tango møter vi fem tangopar fra Buenos Aires, alle med hver sin særegne dansestil, et par samtidsdansere og fem argentiske tangomusikere. Den prisvinnende koreografen Sidi Larbi Cherkaoui tar ideen om en tradisjonell milonga – de sene dansekveldene i Buenos Aires' intime barer – og setter den inn på en annen scene. I dette møtet oppstår en fascinerende tangoforestilling for det 21. århundre, som med dette får sin norgespremiere.</p>"
              + "<p>«Alle mine arbeider handler om forhold, og i tango fant jeg en form for magi i kommunikasjonen mellom mennesker. Bare ved å ta på hverandre, forteller disse parene hverandre alt mulig», sier koreografen Cherkaoui, som reiser verden rundt for å jobbe med dansere fra ulike kulturer. Han har vunnet en lang rekke priser, er blitt utpekt til Årets koreograf av magasinet Tanz to ganger, og i 2012 bar han ansvaret for koreografien i Joe Wrights film Anna Karenina.</p>"
              + "<p>Dette er en Sadler's Wells London-produksjon. </p>"
            }
          ]
        },
        {
          title: 'Nyttårskonsert',
          subtitle: 'Med Miloš Karadaglić, Bjørn Kruse, mm.',
          teaser: 'Lenge før OL-karnevaliet i Rio går av stabelen sommeren 2016, tjuvstarter Filharmonien feiringen med en latin-amerikansk festkonsert.',
          video: '<iframe width="100%" height="344" src="https://www.youtube.com/embed/ULJy6yHadUg" frameborder="0" allowfullscreen></iframe>',
          image: 'images/milos.jpg',
          infoLink: 'http://www.detnorsketeatret.no/framsyningar/brand/',
          buyLink: 'http://www.detnorsketeatret.no/framsyningar/brand/',
          scene: 'filharmonien',
          infoSections: [
            {
              type: 'person',
              image: 'images/people/milos.jpg',
              title: 'Miloš Karadaglić',
              subtitle: 'Gitarist',
            },
            {
              type: 'person',
              image: 'images/people/svein.jpg',
              title: 'Vasily Petrenko',
              subtitle: 'Dirigent',
            }
          ]
        },
        {
          title: "Verdiløse menn",
          subtitle: "En musikal basert på Joachim Nielsens tekst og musikk",
          teaser: "av Christopher Nielsen",
          video: '',
          image: 'images/verdilose-menn.jpg',
          backImage: 'images/verdilose-3.jpg',
          infoLink: 'http://www.nationaltheatret.no/Verdil%C3%B8se+menn.b7C_wJLK3q.ips',
          buyLink: 'http://www.nationaltheatret.no/no/program/Verdil%C3%B8se+menn+%2F+Gatejuristen.b7C_wRDKWK.ips?do=buy&perindex=16910',
          scene: 'national',
          infoSections: [
            {
              type: "image",
              image: "images/verdilose2.jpg",
              text: "Goggen sitter alene hjemme, er selskapssjuk og konstaterer at gamle kompiser har fått seg jobb eller dame. "
                + "Kleggen er imidlertid ledig, og dukker etter hvert opp med en six-pack med øl, med Knugern på slep, og med "
                + "litt av en nyhet. Slik åpner Verdiløse menn, Christopher Nielsens musikal, der låtene til Joachim Nielsen "
                + "flettes inn i historier om damer og dop, tigging og øl, døde venner og udødelig kameratskap. "
                + "Stort lenger off Broadway går det neppe an å komme!"
            }
          ]
        },
        {
          title: "The Lulu show",
          subtitle: "Urpremiere 19/11 - Centralteatret",
          teaser: "Med Hilde Louise Asbjørnsen",
          video: '<iframe width="100%" height="344" src="https://www.youtube.com/embed/VmOyIaWRIVw" frameborder="0" allowfullscreen></iframe>',
          image: 'images/lulu.jpg',
          infoLink: 'http://oslonye.no/the-lulu-show/',
          buyLink: 'http://www.billettservice.no/search/?keyword=The+Lulu+Show',
          bannerText: 'Premiere',
          scene: 'oslonye',
          infoSections: [
            {
              type: "image",
              image: "images/lulu2.jpg",
              text: "Med selvskrevet manus og finanskrisen som tema inntar Hilde Louise Asbjørnsen Centralteatret med et burleskshow om hemmelige fristelser og gjeldskrise."
            }
          ]
        }
      ];

      function setupSlides(data) {
        var slides = '',
            show;

        document.getElementById('slides-wrapper').innerHTML = '<div id="slider-prev" class="slider-nav slider-pause"></div>'
          + '<div id="slider-viewport"><div id="slides" class="slider-pause"></div></div>'
          + '<div id="slider-next" class="slider-nav slider-pause"></div>';

        for (var i=0; i<data.length; i++) {
          show = data[i];
          slides += '<div class="slide row" id="slide-' + i + '" data-scene="' + show.scene + '">'
            + '<div class="eight columns video"><img class="main-img" src="" data-src="' + show.image + '" alt="Brand" style="width:100%;">';

            if (show.bannerText !== undefined || '') {
              slides += '<div class="special">' + show.bannerText + '</div>';
            }

            slides += '</div><div class="four columns info-sidebar"><h1>' + show.title + '</h1><h4>' + show.subtitle + '</h4><h3>' + show.teaser + '</h3>'
            + '<div class="buttons"><a class="button break" href="' + show.infoLink + '" target="_blank">Les mer</a>'
            + '<a class="button break" href="' + show.buyLink + '" target="_blank">Billetter</a></div></div></div>';
        }

        document.getElementById('slides').innerHTML = slides;
      }

      function setupBackside(show) {
        backside = true;

        var html = '<div class="eight columns"><a id="close" href="#">X</a>';

        html += show.video == '' ? '<img src="' + show.backImage + '">' : show.video;
        html += '</div><div class="four columns info-sidebar">';

        for (var i=0; i<show.infoSections.length; i++) {
          infoSection = show.infoSections[i];
          html += '<div class="row actor">';

          if (infoSection.type == 'person') {
            html += '<div class="three columns"><img src="' + infoSection.image + '" alt="' + infoSection.title + '"></div>'
              + '<div class="eight columns"><h2>' + infoSection.title + '</h2>';

              if (infoSection.description !== undefined || '') {
                html += '<p>' + infoSection.description + '</p>';
              }

            html += '</div>';
          }
          else {
            html += '<div class="row actor info"><img src="' + infoSection.image + '"><p>' + infoSection.description + '</p></div>';
          }

          html += '</div>';
        }

        html += '<div class="buttons"><a class="button break" href="' + show.infoLink + '" target="_blank">Les mer</a>'
        + '<a class="button break" href="' + show.buyLink + '" target="_blank">Billetter</a></div></div></div>';

        document.getElementById('backside-wrapper').innerHTML = html;
        document.getElementById('slides-wrapper').style.display = 'none';
        document.getElementById('backside-wrapper').style.display = 'block';

        document.getElementById('close').addEventListener('click', close);

        var iframe = document.getElementsByTagName('iframe')[0];
        if (iframe !== undefined) {
          iframe.src = iframe.src + '?autoplay=1';
        }

        setupLinks();
      }

      function setupSlider() {
        var slideNext = document.getElementById('slider-next'),
            slidePrev = document.getElementById('slider-prev'),
            video = document.getElementsByClassName('video');

        slidesWrap = document.getElementById('slides');
        slides = document.getElementsByClassName('slide');
        sliderPausers = [slidesWrap, slideNext, slidePrev];

        slidesWrap.style.width = slides.length * 940 + 'px';

        updateActiveLogo(currentSlide);

        slideNext.addEventListener('click', changeSlide);
        slidePrev.addEventListener('click', changeSlide);

        for (var i=0; i<sliderPausers.length; i++) {
          sliderPausers[i].addEventListener('mouseover', function() {
            paused = true;
          });

          sliderPausers[i].addEventListener('mouseout', function() {
            paused = false;
          });
        }

        for (var i=0; i<video.length; i++) {
          video[i].addEventListener('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            setupBackside(shows[currentSlide]);
          });
        }

        setupLinks();
      }

      function changeSlide(event) {
        if (event !== undefined) {
          event.preventDefault();
          event.stopPropagation();
        }

        var directionForward = event === undefined || event.srcElement.id == 'slider-next',
            next = directionForward ? (currentSlide + 1) % slides.length : (currentSlide - 1) % slides.length;

        if (next < 0) {
          next = slides.length - 1;
        }

        loadImage(next);
        loadImage(next + 1);

        updateActiveLogo(next);

        currentSlide = next;
        slidesWrap.style.left = (next * -940) + 'px';
      }

      function updateActiveLogo(active) {
        var logos = document.getElementsByClassName('logo');
        for (var i=0; i<logos.length; i++) {
          logos[i].className = 'logo';
        }

        document.getElementById(slides[active].getAttribute('data-scene')).className += ' active';
      }

      function loadImage(slideNumber) {
        if (slideNumber >= slides.length) {
          return;
        }

        var image = document.querySelector('#slide-' + slideNumber + ' .main-img'),
            src = image.getAttribute('data-src');

        image.src = src;
      }

      function close(event) {
        event.preventDefault();
        event.stopPropagation();

        document.getElementById('backside-wrapper').innerHTML = '';
        document.getElementById('slides-wrapper').style.display = 'block';
        backside = false;
      }

      function openWindow() {
        window.open('http://pascenen.savant.no', 'new_window');
      }

      function setupLinks() {
        var links = document.getElementsByTagName('a');
        for (var i=0; i<links.length; i++) {
          if (links[i].getAttribute('href') != '#' || '' || undefined) {
            links[i].addEventListener('click', function(event) {
              event.preventDefault();
              event.stopPropagation();

              window.open(event.srcElement.href, 'new_window');
            });
          }
        }
      }

      var currentSlide = 0,
          paused = false,
          backside = false,
          slides,
          slidesWrap,
          sliderPausers;

      setupSlides(shows);
      setupSlider();

      loadImage(0);
      loadImage(1);

      setInterval(function() {
        if (!paused && !backside) {
          changeSlide();
        }
      }, 5000);
    </script>
</body>
</html>
