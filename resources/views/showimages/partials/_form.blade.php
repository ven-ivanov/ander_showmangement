<section class="form-section">
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns">
                <h4>Bilder (til høyre i annonsen)</h4>
            </div>
        </div>
        <div class="small-12 columns">
            <table id="image-uploader-table">
                <tr class="image-uploader" style="display: none;">
                    <td class="image">
                        <img class="image-preview" src="/images/picture.svg" alt="Placeholder picture">
                    </td>
                    <td class="image-upload">
                        <input type="file" name="image">
                        <a href="#" class="button btn-image-upload">Last opp bilde</a>
                    </td>
                    <td class="description"><input type="text" placeholder="Navn" name="title[]" id="title"> <textarea placeholder="Billedtekst" name="description[]" id="description"></textarea></td>
                    <td class="actions"><a href="#" class="remove-image-uploader">Fjern</a></td>
                    <input type="hidden" name="file_path[]" id="file_path"/>
                    <input type="hidden" name="id[]" id="id"  value="0"/>
                </tr>
                @if(!$show->showImages->count())
                    {{--<tr class="image-uploader">
                        <td class="image">
                            <img class="image-preview" src="/images/picture.svg" alt="Placeholder picture">
                        </td>
                        <td class="image-upload">
                            <input type="file" name="image">
                            <a href="#" class="button btn-image-upload">Last opp bilde</a>
                        </td>
                        <td class="description"><input type="text" placeholder="Navn" name="title[]" id="title"> <textarea placeholder="Billedtekst" name="description[]" id="description"></textarea></td>
                        <td class="actions"><a href="#" class="remove-image-uploader">Fjern</a></td>
                        <input type="hidden" name="file_path[]" id="file_path"/>
                        <input type="hidden" name="id[]" id="id"  value="0"/>
                    </tr>--}}
                 @else
                    @foreach($show->showImages as $showImage)
                        <tr class="image-uploader">
                            <td class="image">
                                <img class="image-preview" src="/uploads/{{$showImage->image_file}}" alt="Placeholder picture">
                            </td>
                            <td class="image-upload">
                                <input type="file" name="image">
                                <a href="#" class="button btn-image-upload">Last opp bilde</a>
                            </td>
                            <td class="description">
                                <input type="text" placeholder="Navn" name="title[]" id="title" value="{{$showImage->title}}">
                                <textarea placeholder="Billedtekst" name="description[]" id="description">{{$showImage->description}}</textarea>
                            </td>
                            <td class="actions"><a href="#" class="remove-image-uploader">Fjern</a></td>
                            <input type="hidden" name="file_path[]" id="file_path" value="{{$showImage->image_file}}"/>
                            <input type="hidden" name="id[]" id="id"  value="{{$showImage->id}}" />
                        </tr>
                    @endforeach
                 @endif

            </table>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns">
                <a href="#" id="add-image-uploader">+ Legg til bilde</a>
            </div>
        </div>
    </div>
</section>
