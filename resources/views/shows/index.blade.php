@extends('app')

@section('content')
    @include('admin-header')
    <link rel="stylesheet" href="{{ URL::asset('/css/admin.css') }}">

    <section id="playorder">
        <iframe src="/ad" width="1000" height="535" frameBorder="0" class="padded"></iframe>
    </section>

    <section class="publish">
        <div class="row">
            <div class="small-12 columns">
                <a href="/shows/create?org={{ $orgId }}" class="button">Opprett forestilling</a>

                @if ($user->role == 'administrator')
                  <select name="org" id="org-select">
                    @foreach ($orgs as $org)
                    <option value="{{ $org->id }}" @if ($org->id == $orgId) selected="selected" @endif>{{ $org->title }}</option>
                    @endforeach
                  </select>
                @endif
            </div>
        </div>
    </section>

    <section id="playorder">
        <input type="hidden" name="org" id="org" value="{{ $orgId }}">

        <div class="row">
            <table>
              <tbody id="shows-order">
                @foreach ($shows as $show)
                <tr data-show-id="{{ $show->id }}">
                    <td><input name="promote" id="promote" type="checkbox" @if ($show->promoted_to_ad == 1) checked="checked" @endif></td>
                    <td class="image"><img src="/img/{{ $show->main_image }}?w=184&amp;h=103&amp;fit=crop" alt="Brand" /></td>
                    <td class="title"><h4>{{ $show->title }}</h4><p>{{ $show->subtitle }}</p></td>
                    <td class="actions">
                        <a>Vis datoer
                            <div class="auto-kal" data-kal="format: 'DD.MM.YYYY', mode: 'multiple', selected: [{{ $show->dates_formatted }}]"></div>
                        </a>
                        <a href="{{ URL::route('shows.edit', $show) }}" class="second">Rediger</a>
                        {!! Form::open(array('style' => 'display:inline','method' => 'DELETE',  'route' => array('shows.destroy', $show->id))) !!}
                        <a href="/{{ URL::route('shows.destroy', $show->id) }}" class="second delete-show" type="submit">Fjern</a>
                        {!! Form::close() !!}

                    </td>
                    <td class="draggable"><img src="/images/anchor.svg" alt="change order" class="anchor"></td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </section>

    <section class="publish">
        <div class="row">
            <div class="small-12 columns">
                <a href="/shows/create?org={{ $orgId }}" class="button">Opprett forestilling</a>
            </div>
        </div>
    </section>
@endsection
