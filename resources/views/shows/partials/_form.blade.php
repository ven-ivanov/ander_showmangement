    <section class="form-section">
        <div class="row publish">
            <div class="small-12 columns">
                <div class="small-12 columns">
                    <a class="button create_show_link">Publiser</a>
                    <a href="/shows" class="button second">Avbryt</a>
                </div>
            </div>
        </div>
        @if ($errors->any())
            <div class="row publish errors">
                <div class="small-12 columns">
                    <div class="small-12 columns flash alert-danger">
                        @foreach ( $errors->all() as $error )
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </section>
    <section class="form-section">
        <div class="row">
            <div class="small-12 columns">
                <div class="medium-6 small-12 columns">
                    <label>Tittel
                        <input type="text" placeholder="Forestilling tittel" name="title" id="title" value="{{ (Input::old('title')) ? Input::old('title') : $show->title }}" class="@if ($errors->first('title')) error @endif">
                    </label>
                    <label>Undertittel
                        <input type="text" placeholder="Dramatiker, skribent, opprettshaver o.l." name="subtitle" id="subtitle" value="{{ (Input::old('subtitle')) ? Input::old('subtitle') : $show->subtitle }}" class="@if ($errors->first('subtitle')) error @endif">
                    </label>
                </div>
                <div class="medium-6 small-12 columns">
                    <label>Banner-tekst
                        <input type="text" placeholder="Spesialinfo om tilbud e.l." name="banner_text" id="banner_text" value="{{ (Input::old('banner_text')) ? Input::old('banner_text') : $show->banner_text }}" class="@if ($errors->first('banner_text')) error @endif">
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="small-12 columns">
                <div class="medium-6 small-12 columns">
                    <label class="tall">Informasjon
                        <textarea placeholder="Kort beskrivelse av forestillingen" name="description" class="@if ($errors->first('description')) error @endif">{{ (Input::old('description')) ? Input::old('description') : $show->description }}</textarea>
                    </label>
                </div>
                <div class="medium-6 small-12 columns">
                    <label>Lenke til mer info
                        <input type="text" placeholder="Lenke til forestilling hos scenehus" name="information_link" id="information_link" value="{{ (Input::old('information_link')) ? Input::old('information_link') : $show->information_link }}" class="@if ($errors->first('information_link')) error @endif">
                    </label>
                    <label>Lenke til billettsalg
                        <input type="text" placeholder="Lenke til billett - betalingsløsning" name="tickets_link" id="tickets_link" value="{{ (Input::old('tickets_link')) ? Input::old('tickets_link') : $show->tickets_link }}" class="@if ($errors->first('tickets_link')) error @endif">
                    </label>
                </div>
            </div>
        </div>
    </section>

    <section class="form-section">
        <div class="row">
            <div class="medium-6 small-12 columns main-uploader">
                <div class="small-12 columns"><h4>Hovedbilde</h4></div>
                <div class="small-6 columns">
                    @if ($show->main_image)
                        <img class="image-preview" src="/uploads/{{$show->main_image}}" alt="Placeholder image">
                    @else
                        <img class="image-preview" src="/images/picture.svg" alt="Placeholder image">
                    @endif
                    <input type="hidden" name="main_image" id="main_image" value="{{$show->main_image}}"/>
                </div>
                <div class="small-6 columns">
                    <input type="file" name="image" class="@if ($errors->first('image')) error @endif">
                    <a href="#" class="button btn-main-image-upload">Last opp bilde</a>
                </div>
            </div>
            <div class="medium-6 small-12 columns">
                <div class="small-12 columns"><h4>Video</h4></div>
                <div class="small-6 columns">
                    <!-- <img src="/images/picture.svg" alt="Placeholder image"> -->
                </div>
                <div class="small-12 columns">
                    <label>Videolenke
                        <input type="text" placeholder="Url eller link til video" name="video_link" id="video_link" value="{{ (Input::old('video_link')) ? Input::old('video_link') : $show->video_link }}" class="@if ($errors->first('video_link')) error @endif">
                    </label>
                </div>
            </div>
        </div>
</section>
