@extends('app')
@section('content')
    @include('admin-header')

    <link rel="stylesheet" href="{{ URL::asset('/css/admin.css') }}">
    {!! Form::model(new App\Show, ['route' => ['shows.store'], 'class'=>'show_main_form', 'id'=>'create_show_form']) !!}
         @include('shows/partials/_form', ['submit_text' => 'Create Show'])
         
         <input type="hidden" name="org" value="{{ $orgId }}">
    {!! Form::close() !!}

    {!! Form::open(array('action'=>'ShowImagesController@saveImage', 'method' => 'post', 'class'=>'show_image_form')) !!}
        @include('showimages/partials/_form')
    {!! Form::close() !!}

    {!! Form::open(array('action'=>'ShowPersonsController@saveImage', 'method' => 'post', 'class'=>'show_people_form')) !!}
    @include('showpersons/partials/_form')
    {!! Form::close() !!}

    <section class="form-section">
        <div class="row">
            <div class="small-12 columns">
                <div class="small-12 columns">
                    <h4>Datoer</h4>
                    <input type="hidden" name="show_dates" id="date-input" value="">
                    <div id="date"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="form-section">
        <div class="row publish">
            <div class="small-12 columns">
                <div class="small-12 columns">
                    <a class="button create_show_link">Publiser</a>
                    <a href="/shows" class="button second">Avbryt</a>
                </div>
            </div>
        </div>
    </section>

@endsection
