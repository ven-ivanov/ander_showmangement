<section class="form-section">
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns">
                <h4>Medvirkende</h4>
            </div>
        </div>
        <div class="small-12 columns">
            <table id="person-uploader-table">
                <tr class="person-uploader" style="display: none;">
                    <td class="image">
                        <img src="/images/picture11.svg" class="image-preview" alt="Placeholder picture">
                    </td>
                    <td class="image-upload">
                        <input type="file" name="image">
                        <a href="#" class="button btn-people-upload">Last opp bilde</a>
                    </td>
                    <td class="description"><input type="text" placeholder="Navn" name="title[]" id="title"> <textarea placeholder="Beskrivelse" name="description[]" id="description"></textarea></td>
                    <td class="actions"><a href="#" class="remove-person-uploader">Fjern</a></td>
                    <input type="hidden" name="file_path[]" id="file_path"/>
                    <input type="hidden" name="id[]" id="id" value="0"/>
                </tr>
                @if(!$show->showPeople->count()>0)
                    {{--<tr class="person-uploader">
                        <td class="image">
                            <img src="/images/picture11.svg" class="image-preview" alt="Placeholder picture">
                        </td>
                        <td class="image-upload">
                            <input type="file" name="image">
                            <a href="#" class="button btn-people-upload">Last opp bilde</a>
                        </td>
                        <td class="description"><input type="text" placeholder="Navn" name="title[]" id="title"> <textarea placeholder="Beskrivelse" name="description[]" id="description"></textarea></td>
                        <td class="actions"><a href="#" class="remove-person-uploader">Fjern</a></td>
                        <input type="hidden" name="file_path[]" id="file_path"/>
                        <input type="hidden" name="id[]" id="id" value="0"/>
                    </tr>--}}
                @else
                    @foreach($show->showPeople as $showPerson)
                        <tr class="person-uploader">
                            <td class="image">
                                <img src="/uploads/{{$showPerson->image_file}}" class="image-preview" alt="Placeholder picture">
                            </td>
                            <td class="image-upload">
                                <input type="file" name="image">
                                <a href="#" class="button btn-people-upload">Last opp bilde</a>
                            </td>
                            <td class="description"><input type="text" placeholder="Navn" name="title[]" id="title" value="{{$showPerson->title}}"> <textarea placeholder="Beskrivelse" id="description" name="description[]">{{$showPerson->description}}</textarea></td>
                            <td class="actions"><a href="#" class="remove-person-uploader">Fjern</a></td>
                            <input type="hidden" name="file_path[]" id="file_path" value="{{$showPerson->image_file}}"/>
                            <input type="hidden" name="id[]" id="id" value="{{$showPerson->id}}"/>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns">
                <a href="#" id="add-person-uploader">+ Legg til person</a>
            </div>
        </div>
    </div>
</section>
