<ul class="small-block-grid-1 medium-block-grid-2">
  @foreach ($shows as $show)
  <li class="show">
    <div class="small-12 columns">
      <div class="video-container">
        <img src="img/{{ $show->main_image }}?w=450&amp;h=253&amp;fit=crop" alt="Featured">
      </div>
    </div>
    <div class="small-12 large-7 columns text-left">
      <div class="small-12 title">
        <h1>{{ $show->title }}</h1>
      </div>
      <div class="small-12 author">
        {{ $show->subtitle }}
      </div>
    </div>
    <div class="small-12 large-5 columns right text-right">
      <div class="small-12">
        <a href="{{ $show->tickets_link }}" target="_blank" class="button expand">Billetter</a>
      </div>
      <div class="small-12">
        <a href="{{ $show->information_link }}" target="_blank" class="button expand">Les mer</a>
      </div>
    </div>
  </li>
  @endforeach
</ul>
