<link rel="stylesheet" href="{{ URL::asset('/css/app.css') }}">
@extends('app')

@section('content')
	@include('header')
	<section id="selector">
		<div class="fullwidth">
			<form id="search-form">
				<div class="row">
					<div class="small-12 medium-3 columns">
						<input type="text" placeholder="Velg dato" class="date" name="date">
					</div>
					<div class="small-12 medium-3 columns">
						<label>
							<select class="scene" name="org">
								<option value="">Velg scenehus</option>
								@foreach ($orgs as $org)
								<option value="{{ $org->id }}">{{ $org->title }}</option>
								@endforeach
							</select>
						</label>
					</div>
					<div class="small-12 medium-3 columns left">
						<label>
							<select class="show-select" name="show">
								<option value="">Velg forestilling</option>
								@foreach ($shows as $show)
								<option value="{{ $show->id }}">{{ $show->title }}</option>
								@endforeach
							</select>
						</label>
					</div>
				</div>
			</form>
		</div>
	</section>

	<section id="more">
		<div class="row collapse" id="shows-list">
			@include('shows', ['shows' => $shows])
		</div>
	</section>
@endsection
