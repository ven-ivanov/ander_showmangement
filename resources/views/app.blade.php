<!doctype html>
<html class="no-js" lang="en" >
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>På Scenen</title>
	<!-- build:css css/libraries.min.css -->
	<!-- bower:css -->
	<link rel="stylesheet" href="/bower_components/chipersoft-kalendae/build/kalendae.css">
	<!-- endbower -->
	<!-- endbuild -->
	<!-- build:css css/app.min.css -->

	<link rel="stylesheet" href="/css/app_override.css">
	<!-- endbuild -->
	<!-- build:css css/kalendae.min.css -->
	{{--<link rel="stylesheet" href="/bower_components/chipersoft-kalendae/build/kalendae.css">--}}
	<!-- endbuild -->
	<!-- build:js js/vendor/modernizr.min.js -->
	<script src="/bower_components/modernizr/modernizr.js"></script>
	<script src="/bower_components/moment/min/moment.min.js"></script>
	<!-- endbuild -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400' rel='stylesheet' type='text/css'>
</head>
<body>
@yield('content')
<script src="/bower_components/jquery/dist/jquery.js"></script>
<!-- build:js js/vendor/kalendae.min.js -->
<script src="/bower_components/jquery/dist/jquery.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script src="/bower_components/fastclick/lib/fastclick.js"></script>
<script src="/bower_components/jquery.cookie/jquery.cookie.js"></script>
<script src="/bower_components/moment/moment.js"></script>
<script src="/bower_components/chipersoft-kalendae/build/kalendae.min.js"></script>
<!-- bower:js -->
<!-- endbower -->
<!-- endbuild -->
<!-- build:js js/vendor/foundation.min.js -->
<script src="/bower_components/foundation/js/foundation/foundation.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.abide.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.accordion.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.alert.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.clearing.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.dropdown.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.equalizer.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.interchange.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.joyride.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.magellan.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.offcanvas.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.orbit.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.reveal.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.slider.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.tab.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.tooltip.js"></script>
<script src="/bower_components/foundation/js/foundation/foundation.topbar.js"></script>
<!-- endbuild -->
<!-- build:js js/bower_components/moment/min/lang/nb.js -->
<script src="/bower_components/moment/min/lang/nb.js"></script>
<!-- endbuild -->

<script src="/bower_components/Sortable/Sortable.min.js"></script>

<!-- build:js js/app.min.js -->
<script src="/js/app.js"></script>
<!-- endbuild -->
</body>
</html>
