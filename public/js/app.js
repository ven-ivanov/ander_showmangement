'use strict';

var app = (function(document, $) {
	var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_init = function() {
			$(document).foundation();
            // needed to use joyride
            // doc: http://foundation.zurb.com/docs/components/joyride.html
            $(document).on('click', '#start-jr', function () {
                $(document).foundation('joyride', 'start');
            });
			_userAgentInit();
		};
	return {
		init: _init
	};
})(document, jQuery);

(function() {
	app.init();
})();

$(document).ready(function() {
	moment.lang('nb');
	var upload_index = 0;

	$('#search').click(searchForShows);
	$('#search-form input, #search-form select').change(searchForShows);

	function searchForShows() {
		$.ajax({
			url: '/get-selection',
			method: 'POST',
			data: $('#search-form').serialize(),
			success: function(data) {
				$('#shows-list').html(data);
			}
		});
	}

	$('.date').kalendae({
		format: 'DD.MM.YYYY',
		mode: 'multiple',
		weekStart: 1
	});

	$('a.create_show_link').click(function(){
		//add show Image Data
		var showImageForm = ($('.show_image_form').serialize());
		var showPeopleForm =($('.show_people_form').serialize());
		var show_dates = $('#date-input').val();
		/*var showImageForm = $('.show_image_form').serialize();
		console.log(showImageForm);
		var showPeopleForm =$('.show_people_form').serialize();*/

		//show dates
		$('<input>').attr({
			type: 'hidden',
			id: 'show_dates',
			name: 'show_dates',
			value: show_dates
		}).appendTo('#create_show_form');

		$('<input>').attr({
			type: 'hidden',
			id: 'image_form',
			name: 'image_form',
			value: showImageForm
		}).appendTo('#create_show_form');

		$('<input>').attr({
			type: 'hidden',
			id: 'person_form',
			name: 'person_form',
			value: showPeopleForm
		}).appendTo('#create_show_form');
		//add show People Data
		$('#create_show_form').submit();
	});


	//Image Uploader
	$('#add-image-uploader').click(function() {
		var newBlock = $('.image-uploader').first().clone(true);
		newBlock.css('display','block');
		$('#file_path',newBlock).val('');
		$('#title',newBlock).val('');
		$('#description',newBlock).val('');
		$('.image-preview',newBlock).attr('src','/images/picture.svg');
		$('#id',newBlock).val(0);

		newBlock.appendTo($('#image-uploader-table'));
		return false;
	});

	$('#image-uploader-table').on('click','.remove-image-uploader',function(){
		$(this).closest('.image-uploader').remove();
		return false;
	});

	//Person Uploader
	$('#add-person-uploader').click(function(){
		var newBlock=  $('.person-uploader:eq(0)').clone(true);
		newBlock.css('display','block');
		$('#file_path',newBlock).val('');
		$('#id',newBlock).val(0);
		$('#title',newBlock).val('');
		$('#description',newBlock).val('');
		$('.image-preview',newBlock).attr('src','/images/picture11.svg');

		newBlock.appendTo($('#person-uploader-table'));
		return false;
	});

	$('#person-uploader-table').on('click','.remove-person-uploader',function(){
		$(this).closest('.person-uploader').remove();
		return false;
	});
	//General Uploader

	var optionsImage = {
		beforeSubmit:  showRequestImage,
		success:       showResponseImage,
		dataType: 'json'
	};

	var optionsPerson = {
		beforeSubmit:  showRequestPerson,
		success:       showResponsePerson,
		dataType: 'json'
	};

	$.ajaxPrefilter(function(options, originalOptions, xhr) {
		var token = $('meta[name="csrf_token"]').attr('content');

		if (token) {
			return xhr.setRequestHeader('X-XSRF-TOKEN', token);
		}
	});

	//show people main image uploader
	$('body').delegate('.btn-main-image-upload','click', function(){
		//upload image
		upload_index = $('.main-uploader').index($(this).closest('.main-uploader'));
		$('.main-uploader').each(function(index,element){
			if(index != upload_index){
				$(element).find(':file').val('');
			}
		});
		var patch = false;
		if($("input[name='_method']",$('.show_main_form')[0]).val()=='PATCH')
			patch = true;
		if(patch){
			$("input[name='_method']",$('.show_main_form')[0]).val('POST');
		}

		$.ajax(
			{	url: '/upload/mainimage',
				method: 'POST',
				processData: false,
				contentType: false,
				data: new FormData($('.show_main_form')[0]),
				beforeSend: function (xhr) {
					var token = $('meta[name="csrf_token"]').attr('content');

					if (token) {
						return xhr.setRequestHeader('X-XSRF-TOKEN', token);
					}
				}
			})
			.done(function(response, statusText, xhr){
				var $form = $('.show_main_form')[0];
				if (response.success == false) {
					var arr = response.errors;
					$.each(arr, function (index, value) {
						if (value.length != 0) {
							$("#validation-errors").append('<div class="alert alert-error"><strong>' + value + '</strong><div>');
						}
					});
					$("#validation-errors").show();
				} else {
					//we need to get index
					$('.main-uploader').eq(0).find('#main_image').val(response.file);
					//show preview
					$('.main-uploader').eq(0).find('.image-preview')[0].src = response.preview;
				}
				if(patch){
					$("input[name='_method']",$('.show_main_form')[0]).val('PATCH');
				}
			})
		return false;

	});

	//show image uploader
	$('body').delegate('.btn-image-upload','click', function(){
		//upload image
		upload_index = $('tr.image-uploader').index($(this).closest('tr.image-uploader'));
		$('tr.image-uploader').each(function(index,element){
			if(index != upload_index){
				$(element).find(':file').val('');
			}
		});
		$(this).closest('.show_image_form').ajaxForm(optionsImage).submit();

		return false;
	});

	//show people image uploader
	$('body').delegate('.btn-people-upload','click', function(){
		//upload image
		upload_index = $('tr.person-uploader').index($(this).closest('tr.person-uploader'));
		$('tr.person-uploader').each(function(index,element){
			if(index != upload_index){
				$(element).find(':file').val('');
			}
		});
		$(this).closest('.show_people_form').ajaxForm(optionsPerson).submit();
		return false;
	});

	function showRequestImage(formData, jqForm, options) {
		$("#validation-errors").hide().empty();
		$("#output").css('display','none');
		return true;
	}

	function showResponseImage(response, statusText, xhr, $form) {
		if (response.success == false) {
			var arr = response.errors;
			$.each(arr, function (index, value) {
				if (value.length != 0) {
					$("#validation-errors").append('<div class="alert alert-error"><strong>' + value + '</strong><div>');
				}
			});
			$("#validation-errors").show();
		} else {
			//we need to get index
			$form.find('tr.image-uploader').eq(upload_index).find('#file_path').val(response.file);
			//show preview
			$form.find('tr.image-uploader').eq(upload_index).find('.image-preview')[0].src = response.preview;
		}
	};

	function showRequestMainImage(formData, jqForm, options) {
		$("#validation-errors").hide().empty();
		$("#output").css('display','none');
		return true;
	}
	function showResponseMainImage(response, statusText, xhr, $form) {
		if (response.success == false) {
			var arr = response.errors;
			$.each(arr, function (index, value) {
				if (value.length != 0) {
					$("#validation-errors").append('<div class="alert alert-error"><strong>' + value + '</strong><div>');
				}
			});
			$("#validation-errors").show();
		} else {
			//we need to get index
			$form.find('tr.image-uploader').eq(upload_index).find('#file_path').val(response.file);
			//show preview
			$form.find('tr.image-uploader').eq(upload_index).find('.image-preview')[0].src = response.preview;
		}
	};

	function showRequestPerson(formData, jqForm, options) {
		$("#validation-errors").hide().empty();
		$("#output").css('display','none');
		return true;
	}

	function showResponsePerson(response, statusText, xhr, $form) {
		if (response.success == false) {
			var arr = response.errors;
			$.each(arr, function (index, value) {
				if (value.length != 0) {
					$("#validation-errors").append('<div class="alert alert-error"><strong>' + value + '</strong><div>');
				}
			});
			$("#validation-errors").show();
		} else {
			//we need to get index
			$form.find('tr.person-uploader').eq(upload_index).find('#file_path').val(response.file);
			//show preview
			$form.find('tr.person-uploader').eq(upload_index).find('.image-preview')[0].src = response.preview;
		}
	};

	// Datepicker on show edit
	var kalendae = $('#date').kalendae({
		format: 'DD.MM.YYYY',
		months: 4,
		mode: 'multiple',
		attachTo: document.getElementById('date'),
		subscribe: {
       'change': function (date) {
					$('#date-input').val($('#date').data('kalendae').getSelected());
				}
     }
	});

	if ($('#date').length > 0) {
			$('#date').data('kalendae').setSelected($('#date-input').val());
	}

	// Draggable shows on admin front page
	var el = document.getElementById('shows-order');
	if (el !== null) {
		var sortable = Sortable.create(el, {
			handle: '.draggable',
			onUpdate: function(event) {
				var shows = $('#shows-order tr'),
						order = [];

				for (var i=0; i<shows.length; i++) {
					order.push($(shows[i]).attr('data-show-id'));
				}

				$.ajax({
					url: '/shows/order',
					method: 'POST',
					data: { order: order, org: $('#org').val() },
					success: function() {
						refreshPreview();
					}
				})
			}
		});
	}

	var promoted = $('input[name="promote"]:checked'),
			unpromoted = $('input[name="promote"]:not(:checked)');

	if (promoted.length >= 3) {
			unpromoted.attr('disabled', 'disabled');
	}

	// Promoting of shows to ad
	$('input[name="promote"]').change(function() {
		var promoted = $('input[name="promote"]:checked'),
				unpromoted = $('input[name="promote"]:not(:checked)');

		if (promoted.length >= 3) {
			unpromoted.attr('disabled', 'disabled');
		}
		else {
			unpromoted.removeAttr('disabled');
		}

		$.ajax({
			url: '/shows/promote',
			method: 'POST',
			data: {
				id: $(this).closest('tr').attr('data-show-id'),
				status: $(this).is(':checked') ? 1 : 0
			},
			success: function() {
				refreshPreview();
			}
		});
	});

	// Refresh the preview of ad.
	function refreshPreview() {
		var random = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 25);
		$('iframe').attr('src', '/ad?id=' + random);
	}

	$('.delete-show').on('click', function(e)
	{
		// Stop anchor tag from trying to execute as a link
		e.preventDefault();
		if(confirm('Are you sure to delete this show?')) {
			$(this).closest('form').submit();
		}
	});

	$('#org-select').change(function() {
		window.location.href = '/shows/admin/' + $(this).val();
	});
});
